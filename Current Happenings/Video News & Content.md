## Video News & Content

<!--When adding new entries, for each table row, start a new line, and for each table cell, append a ` | ` followed directly by the table cell data-->
<!--Also, append a `| :--:` to the 2nd line for each new entry-->

**Vlogger** | **Release Schedule** | **Latest Release**
:---: | :---: | :---:
<a href="https://www.youtube.com/channel/UCj6WU3IdNXHuwqv6WbcUH2Q" target="_blank"><img src="http://a.pomf.se/llbbnc.png"><br>**Sockarina**</a> | Daily | <a href="https://www.youtube.com/watch?v=Dr4duyIcxcw&list=UUj6WU3IdNXHuwqv6WbcUH2Q" target="_blank">30 Sep-1 Oct 2014</a>
<a href="https://www.youtube.com/channel/UC1FantLFzgT7FDrWB_7eUKg" target="_blank"><img src="http://a.pomf.se/wuajyk.png"><br>**GamerGate News**</a> | Mon/Wed/Fri | <a href="https://www.youtube.com/watch?v=yEEoyQQhWYk&list=UU1FantLFzgT7FDrWB_7eUKg" target="_blank">29 Sep 2014</a>

**Streamer** | **Next Scheduled Stream** | **Topic**
:---: | :---: | :---:
KingofPol | <a href="http://hitbox.tv/kingofpol" target="_blank">7 AM PDT (PST)/10 AM EDT (EST)/3 PM BDT (GMT) 2 Oct 2014</a> | #GamerGate News Update
TVTokyoBen | <a href="http://hitbox.tv/kingofpol" target="_blank">4 PM PDT (PST)/7 PM EDT (EST)/10 PM BDT (GMT) 2 Oct 2014</a> | GameJournoPros and Mighty No. 9
